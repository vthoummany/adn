﻿using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Threading;

namespace FileMoverService
{
    /// <summary>
    /// The window service 1
    /// </summary>
    public partial class Service1 : ServiceBase
    {
        /// <summary>
        /// The file system watcher.
        /// </summary>
        private static FileSystemWatcher fileSystemWatcher = new FileSystemWatcher("C:\\ADN");

        /// <summary>
        /// Initializes a new instance of the Service1 class.
        /// </summary>
        public Service1()
        {
            InitializeComponent();

        }

        /// <summary>
        /// When the service starts.
        /// </summary>
        /// <param name="args">The string of the argument passed in.</param>
        protected override void OnStart(string[] args)
        {
            fileSystemWatcher.EnableRaisingEvents = true;
            fileSystemWatcher.Created += OnCreate;
            fileSystemWatcher.Changed += OnChange;
            fileSystemWatcher.Renamed += OnRename;
            Console.ReadLine();

            EventLog eventLog = new EventLog();

            eventLog.Source = "Application";

            eventLog.WriteEntry("The Service was started.", EventLogEntryType.Information);
        }

        /// <summary>
        /// When the service stops.
        /// </summary>
        protected override void OnStop()
        {
            // Create log and says the service has stopped.
            EventLog eventLog = new EventLog();

            eventLog.Source = "Application";

            eventLog.WriteEntry("The Service was stopped.", EventLogEntryType.Information);
        }

        /// <summary>
        /// Moves the file.
        /// </summary>
        /// <param name="sourcePath">The path of the file.</param>
        /// <param name="destinationPath">The destination of the file.</param>
        static void MoveFile(string sourcePath, string destinationPath)
        {
            // If the file exists then...
            if (File.Exists(sourcePath))
            {
                // Main thread sleeps
                Thread.Sleep(1000);
            }

            // If the file exists in the destination path.
            if (File.Exists(destinationPath))
            {
                // Delete the file.
                File.Delete(destinationPath);
            }

            // Copy the file from the source path to the destination.
            File.Copy(sourcePath, destinationPath);

        }

        /// <summary>
        /// When the on change happens.
        /// </summary>
        /// <param name="sender">The path of the object path.</param>
        /// <param name="e">The destination path.</param>
        static void OnChange(object sender, FileSystemEventArgs e)
        {
            // New event log.
            EventLog eventLog = new EventLog();

            // Sets the Event log source to the Application
            eventLog.Source = "Application";

            // Writes an entry to the event log.
            eventLog.WriteEntry("File was moved.", EventLogEntryType.Information);
        }
           
        /// <summary>
        /// On the create method.
        /// </summary>
        /// <param name="sender">The destination file.</param>
        /// <param name="e">The target file.</param>
        static void OnCreate(object sender, FileSystemEventArgs e)
        {
            try
            {
                // Move the files passed in.
                MoveFile(e.FullPath, "C:\\target\\" + sender);
            }
            catch
            {
                // Create the event log and write an entry saying it passed.
                EventLog eventLog = new EventLog();

                eventLog.Source = "Application";

                eventLog.WriteEntry("File Could not be moved.", EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// The On rename method.
        /// </summary>
        /// <param name="sender">The destination file path.</param>
        /// <param name="e">The target file path.</param>
        static void OnRename(object sender, FileSystemEventArgs e)
        {
            MoveFile(e.FullPath, "C:\\target\\" + sender);
        }
    }
}
