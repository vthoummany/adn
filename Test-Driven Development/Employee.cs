﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class to represent an employee.
    /// </summary>
    [Serializable]
    public class Employee
    {
        /// <summary>
        /// The wage per hour for the employee.
        /// </summary>
        private decimal hourlyWage;

        /// <summary>
        /// The hours that the employee is scheduled to work.
        /// </summary>
        private int hoursScheduled;

        /// <summary>
        /// Inititalizes a new instance of the Employee class.
        /// </summary>
        /// <param name="hourlyWage">The hourly wage of the employee.</param>
        /// <param name="hoursScheduled">The hours the employee will be scheduled to work.</param>
        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        public Employee()
        {

        }

        /// <summary>
        /// The paycheck that the employee will recieve based on the hourly wage and scheduled hours worked.
        /// </summary>
        public decimal Paycheck { get; set; }

        /// <summary>
        /// The employee does work.
        /// </summary>
        /// <param name="work">The work that the employee will do for the hours scheduled.</param>
        public void DoWork(Job work)
        {
            // If the hoursScheduled are less than the time remaining on the job, 
            // reduce the hours remaining on the job by the hours scheduled, 
            // and set hours scheduled to zero.
            // Else reduce hours scheduled by time remaining, and set time investment for job to 0.
            if(this.hoursScheduled < work.TimeInvestmentRemaining)
            {
                work.TimeInvestmentRemaining = work.TimeInvestmentRemaining - this.hoursScheduled;
                this.Paycheck = (this.hoursScheduled * this.hourlyWage) + this.Paycheck;
                work.JobCost += this.Paycheck * (decimal)1.5; 
                this.hoursScheduled = 0;
            }
            else
            {
                this.hoursScheduled = this.hoursScheduled - work.TimeInvestmentRemaining;
                this.Paycheck = (work.TimeInvestmentRemaining * this.hourlyWage) + this.Paycheck;
                work.JobCost += this.Paycheck * (decimal)1.5;
                work.TimeInvestmentRemaining = 0;
            }
        }
    }
}
