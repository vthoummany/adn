﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The class to represent a job.
    /// </summary>
    [Serializable]
    public class Job
    {
        /// <summary>
        /// Initializes a new instance of the Job class.
        /// </summary>
        /// <param name="timeInvestment">The time needed to complete the job.</param>
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        /// <summary>
        /// Initializes a new instance of the Job class.
        /// </summary>
        public Job()
        {

        }

        /// <summary>
        /// Gets or sets the jobs cost.
        /// </summary>
        public decimal JobCost { get; set; }

        /// <summary>
        /// Gets or sets the time investment that is remaining.
        /// </summary>
        public int TimeInvestmentRemaining { get; set; }

        /// <summary>
        /// Gets true if the job is completed or false depending on whether the time investment remaining is equal to zero or not.
        /// </summary>
        public bool JobCompleted => this.TimeInvestmentRemaining == 0;
    }
}
