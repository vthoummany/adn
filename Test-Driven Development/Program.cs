﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();
            SerializeBusiness(business);
            Console.ReadLine();
        }

        /// <summary>
        /// Serializes the business class.
        /// </summary>
        /// <param name="business">The business class that is serialized.</param>
        static void SerializeBusiness(Business business)
        {
            // Create a binary formatter
            XmlSerializer ser = new XmlSerializer(typeof(Business));

            // Create a file using the passed-in file name
            // Use a using statement to automatically clean up object references
            // and close the file handle when the serialization process is complete
            TextWriter writer = new StreamWriter("C:\\ADN.txt");
            ser.Serialize(writer, business);
            writer.Close();
        }
    }
}
