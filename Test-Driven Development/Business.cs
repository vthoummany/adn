﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    /// <summary>
    /// The business class.
    /// </summary>
    [Serializable]
    public class Business
    {
        /// <summary>
        /// Initializes a new instance of the Business class.
        /// </summary>
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        /// <summary>
        /// Gets or sets the list of employess.
        /// </summary>
        public List<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets the list of jobs.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Adds an employee to the list of employees.
        /// </summary>
        /// <param name="employee">The employee that will be added to the list.</param>
        public void AddEmployee(Employee employee)
        {
            this.Employees.Add(employee);
        }

        /// <summary>
        /// Adds a job to the list of jobs.
        /// </summary>
        /// <param name="job">The job that will be added to the list.</param>
        public void AddJob(Job job)
        {
            this.Jobs.Add(job);
        }

        /// <summary>
        /// Does the work of the jobs. 
        /// </summary>
        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork().
            // If job is completed after work, break from loop.
            foreach (Job j in Jobs)
            {
                if(j.JobCompleted == false)
                {
                    foreach(Employee e in Employees)
                    {
                        e.DoWork(j);
                        
                        if(j.JobCompleted == true)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}
